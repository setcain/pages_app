This project is for test, templates, class-based views, snippets and test.

templates:
https://docs.djangoproject.com/en/2.2/topics/templates/

template tags and filters:
https://docs.djangoproject.com/en/2.2/ref/templates/builtins/

class-based views:
https://docs.djangoproject.com/en/2.2/topics/class-based-views/

test:
https://docs.djangoproject.com/en/2.2/topics/testing/
Jacob Kaplan-Moss "Code without test is broken as designed"


snippets:
Some html code in templates/snippets/

For use it:
add templates/snippets/
and put all your snippets there.
if you want to include some snippet just write:
{% include 'snippets/nav-bar.html' %}
