from .views import AboutPageView, HomePageView
from django.urls import path

urlpatterns = [
    path('', HomePageView.as_view(), name='home_url'),
    path('about/', AboutPageView.as_view(), name='about_url'),
]
